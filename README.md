# Personal Projects

This documentation will briefly guide you how to run my personal projects on your machine.

## Clone the repository

First step is to clone the gitlab repository **Personal Projects**. For this, open your terminal 
and navigate to folder where you want to clone this repository. Then type the 
following command:

    git clone https://gitlab.com/M_Rehman/personal-projects.git

## Creating virtual environment (optional)
The scripts in this repository does not need special python libraries 
to install. You can run them using your existing python `base` or virtual environment  `venv`.

However, use the following commands if you want to create a new venv
for running these scripts:

    python -m venv <your_venv_name>

The above command will generate a new venv in your current directory.
Next step is to activate your venv by using following commands:

For Windows:

    .\<your_env_name>\Scripts\activate

For Linux:

    source <your_venv_name>/bin/activate

## Run the scripts
Now you are ready to run the scripts. Assuming that you are in root 
directory of repository, simply run following command to start the script:

    python Hangman\hangman.py

or

    python Tic_Tac_Toe\tic_tac_toe.py